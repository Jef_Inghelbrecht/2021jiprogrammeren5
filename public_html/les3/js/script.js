const myMap = L.map('mapid').setView([51.2191, 4.40111], 12);

const myBaseMap = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoiam9zZXBoaW5naGVsYnJlY2h0IiwiYSI6ImNrNm5xcXltMTBuOHgzZWx5ZnNxbDY5NmcifQ.2E7Es4nIyOuKQrbU0jgCmw'
}).addTo(myMap);

const request = new XMLHttpRequest()
request.open('GET', 'https://opendata.arcgis.com/datasets/0047052fbbe94040bfe0b9fcad635212_636.geojson', true)

request.onload = function () {
  // Begin accessing JSON data here
  const data = JSON.parse(this.response).features;
  console.log(data);

    data.map(function(row) {
        let elem = document.getElementById('school');
        let p = document.createElement('P');
        let t = document.createTextNode(row.properties.naam);
        p.appendChild(t);
        elem.appendChild(p);
        let marker = L.marker([row.geometry.coordinates[1], row.geometry.coordinates[0]]).addTo(myMap);

    }) 
}

request.send();