var Marvel = require('marvel')

var marvel = new Marvel({ publicKey: "<pubKey>", privateKey: "<privateKey>"})

marvel.characters
  .name("Hulk")
  .get(function(err, resp) {
    if (err) { console.log("Error: ", err) }
    else { console.log(resp) }
  })